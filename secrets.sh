#!/bin/env bash
set -e

function help {
  echo "usage:"  
  echo "  $0 < -l | -s | -h > [ apps ... ]"
  echo ""
  echo "-l: load .env´s"
  echo "-s: store .env´s"
  echo "-h: show this page"
}

function store {
    echo "$0: encrypting files"
    
    for file in ${FILES}; do
      if [ -f ${file} ]; then
        sudo cp ${file} ./.ccrypt-temp-file
        sudo ccrypt -e -f -k ../.env-key ./.ccrypt-temp-file
        sudo mv ./.ccrypt-temp-file.cpt ${file}.cpt
      else
        echo "$0: ${file} dose not exist!"
      fi
    done

    echo "$0: You need to manualy commit the encrypted files!"
}

function load {
    echo "$0: decrypting files"
    
    for file in ${FILES}; do
      if [ -f ${file}.cpt ]; then
        sudo cp ${file}.cpt ./.ccrypt-temp-file
        sudo ccrypt -d -f -k ../.env-key ./.ccrypt-temp-file
        sudo mv ./.ccrypt-temp-file ${file}
      else
        echo "$0: ${file}.cpt dose not exist!"
      fi
    done
}

FILES="synapse/homeserver.yaml synapse/matrix.d4.social.log.config synapse/matrix.d4.social.signing.key"

while getopts "slh" arg; do
  case "${arg}" in
    s) 
      if [ "${cmd}" == "" ]; then
        cmd=store
      else
        echo "$0: store (s) and load (l) are mutual exclusive!"
        exit 1;
      fi
      ;;
    l)
      if [ "${cmd}" == "" ]; then
        cmd=load
      else
        echo "$0: store (s) and load (l) are mutual exclusive!"
        exit 1;
      fi
      ;;
    h)
      help
      exit; 
      ;;
  esac
done
shift $(($OPTIND - 1))

if [ $# -gt 0 ]; then
  APPS="$@"
fi

case "${cmd}" in
  store) store;;
  load) load;;
  *) help;;
esac
